import "./App.css";
import Document from './Document';
import { useEffect, useState } from 'react';


function App() {
  const title = 'Terms and Conditions';
  const button='I Agree';
  const [data, setData] = useState('');

  useEffect(() => {
    const url = "https://jaspervdj.be/lorem-markdownum/markdown.txt";

    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const data = await response.text();
        setData(data) 
      } catch (error) {
        console.log("error", error);
      }
    }

    fetchData();
}, []);


  return (
    <div className="App">
      <Document title={title} content={data} button={button}></Document>
    </div>
  );
}

export default App;
