import { useState } from "react";

function Document(props) {
    const [isRead, setRead] = useState(false);

    function scroll(e){
        const element = e.target;
        const isDown=parseInt(element.scrollHeight) - parseInt(element.scrollTop) === parseInt(element.clientHeight)||(parseInt(element.scrollHeight) - parseInt(element.scrollTop))-1 === parseInt(element.clientHeight);

        if (isDown) {
            setRead(true);
        }
    }

    return (
        <div className="terms">
            <h1 className="title">{props.title}</h1>
            <p className="content" onScroll={e => scroll(e)}>{props.content}</p>
            <button disabled={isRead ? false : true}>{props.button}</button>
        </div>
    )
}

export default Document;